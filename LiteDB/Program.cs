﻿using LiteDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiteDBs
{
    class Program
    {
        static void Main(string[] args)
        {

            //очень удобно, если чего нет - создаст, избавлен от лишнего, про сравнению с mongo при томже функицонале, правильно сохраняет дату и время
            var db = new LiteDB.LiteDatabase(@"MyData.db");
            var users = db.GetCollection<User>("Users");

            //users.EnsureIndex(x => new User { user_name = x.user_name }, true);
            //users.EnsureIndex(x => new User { _id = x._id }, true);
            users.Insert(new BsonValue(DateTime.Now.Ticks.ToString()),  new User
            {
                age = 22,
                user_name = "Bob",
                _id = Guid.NewGuid().ToString().ToLower(),
                company = new Company
                {
                    name = "Otus",
                    startwork = DateTime.Now
                }
            });

            var m = users.Find(x=> true).ToList();

            foreach (var user in m)
                Console.WriteLine(user.user_name + " age: " + user.age.ToString());

            //users.DeleteMany(x => x.user_name == "Bob");
            //users.DeleteAll();

            //sigleUser.company.name = "MegaHard";
            //users.Update(sigleUser);
            //users.UpdateMany(x => new User { age = 3 }, x => x.age > 20);
            //users.DeleteAll();

            //Console.WriteLine();

            Console.ReadKey();
        }
    }

    [MongoDB.Bson.Serialization.Attributes.BsonIgnoreExtraElements]
    public class User
    {
        public string _id { get; set; }
        public string user_name { get; set; }
        public int age { get; set; }
        public Company company { get; set; }
    }

    public class Company
    {
        public string name { get; set; }
        // [BsonDateTimeOptions(Representation = BsonType.String)]
        public DateTime startwork { get; set; }
    }
}
